import pandas as pd

Mindanao = pd.read_csv('./S-012023-026 - Diaz/OtherData/Mindanao.csv')

Grid = pd.read_excel('./S-012023-026 - Diaz/HourlyDemands/Hourly-Demand-per-Grid.xlsx','MINDANAO HOURLY LOAD 2013-2022')

DavaoMain_2 = pd.read_csv('./S-012023-026 - Diaz/OtherData/simplified-datasetes-humidity-temperature-averageLD.csv')
Mindanao_2 = pd.read_csv('./S-012023-026 - Diaz/OtherData/Mindanao_2.csv')

firstRowTrain = 0

lastRowTrain = 1826#-(365*2)

firstRowTest = 1827#-(365*2)

lastRowTest = 2191

#just print every variable here. if you'd like to check it's contents.
#example
#print(DavaoMain_2)