# import sklearn
# Notes: Just remove the (#) sign if you wanna uncomment the commands on the code. 
import numpy as np
import pandas
from ImportData import Grid,Mindanao,DavaoMain_2,Mindanao_2,firstRowTrain,firstRowTest,lastRowTrain,lastRowTest
import math

from sklearn.naive_bayes import MultinomialNB,GaussianNB

from sklearn import svm

SpotPeaksSVM = svm.SVC(kernel='linear') #this is an instance. no need to check its contents unless you'd like to see the SVM object
#if you'd like to check the contents of every variable, just add print(*variable*) to each line
# print(filtered2021) #this is an example

SpotPeaks = MultinomialNB() #declare instance of multinomialNB. No need to touch this. You just need to touch the data
SpotPeaks_Gaussian = GaussianNB() #This is pretty much the same but this is for Gaussian.

filtered2021 = DavaoMain_2 #inherit this from import data. 

#Note: Please be familiar with the data structure using pandas. You can use the pandas documentation to learn more about it.

#print(filtered2021) #just uncomment this to know the data being checked here.

MindanaoSpotPeaks = np.array(Mindanao_2['Spot Peak'].values) #this gets the 'Spot Peak' column from Mindanao_2 variable from ImportData

MindanaoSpotPeaks = MindanaoSpotPeaks[np.logical_not(np.isnan(MindanaoSpotPeaks))] #this just renews and removes values without any value  from the variable above

MindanaoDates = np.array(Mindanao_2['DATE'].values) #same with MindanaoSpotPeaks, just gets the 'DATE' column from Mindanao_2 inherited from ImportData

MindanaoDates = [x for x in MindanaoDates if str(x) != 'nan'] #again. this removes not a number variables in MindanaoDates

MindanaoHourAverage = np.array(Mindanao_2['Ave'].values) #this gets the average values. same process with the others above
MindanaoHourAverage = MindanaoHourAverage[np.logical_not(np.isnan(MindanaoHourAverage))]

trainingData = filtered2021[firstRowTrain:lastRowTrain] #this separates the data. This is the training data.

filtered2021Train = filtered2021[firstRowTrain:lastRowTrain].to_numpy().tolist()  #This is the training data and converts it into an array.
filtered2021Test = filtered2021[firstRowTest:lastRowTest].to_numpy().tolist() #This is the testing data and converts it into an array.

MindanaoSpotPeaksTrain = MindanaoSpotPeaks[firstRowTrain:lastRowTrain] #this also segregates output values to train data
MindanaoSpotPeaksTest = MindanaoSpotPeaks[firstRowTest:lastRowTest] #this also segregates output values to test data
MindanaoDatesTest = MindanaoDates[firstRowTest:lastRowTest] #this also segregates values to test data but for dates

MindanaoAverageTrain = MindanaoHourAverage[firstRowTrain:lastRowTrain] #this also segregates values to train data but for average values
MindanaoAverageTest = MindanaoHourAverage[firstRowTest:lastRowTest] #this also segregates values to test data but for dates



SpotPeaks.fit(filtered2021Train, MindanaoSpotPeaksTrain) #here, we add the data in the model and train the data. no further action needed just make sure the 2 values are the same shape.
SpotPeaks_Gaussian.fit(filtered2021Train, MindanaoSpotPeaksTrain) #same here. I'm just training both of them.

SpotPeaksInput = SpotPeaks.predict(filtered2021Train) #this actually shows the result of our model. We'll use this as our additional input.
SpotPeaksInput_Gaussian = SpotPeaks_Gaussian.predict(filtered2021Train) #same here for Gaussian

#here, we add(append) the values from SpotPeaksInput as an additional field from filtered2021Train
for index,spot in enumerate(SpotPeaksInput):
    filtered2021Train[index].append(spot)

SpotPeaksSVM.fit(filtered2021Train, MindanaoSpotPeaksTrain) #we then retrain it again using SVM. This is considered the last output


