START 

Data Gathering ()
- Get Data from Data set.
- Filter Data to Specific Year 
- Get average values of hours in the data sample 
- segregate input values and output sample values 
- segregate train data and test data 

---------------------------------------------------------------------------------------------

Algorithm Models (GaussianNB and SVM)
- Define GaussianNB and SVM model instances
- Reshape Data to satisfy requirements of algorithm model
- Train model with given set of Train Data acquired from data set.
- use prediction of GaussianNB to train algorithm model of SVM.

Testing, Plotting and T-Test (GaussianNB and SVM)
    Testing
        - Input test data acquired from Data Gathering ()
    
    Plotting
        - Define plot model instance 
        - Define result values for GaussianNB, SVM and Average.
        - For each of the defined values, define model instances for plotting.
        - Include defined values in defined plot model instance

    T-Test
        - Use Plotted Values result to define T-Test
        - Define T-test in plot model instance
    
    -Plot and Print Plot Model Instance
|
v

- Output

END

------------------------------------------------------------------------------------------------

Algorithm Models (MultinomialNB and EEMD)
- Define MultinomialNB and EEMD model instances
- Reshape Data to satisfy requirements of algorithm model
- Train MultinomialNB model with given set of Train Data acquired from data set.
- Use first model instance of EEMD as reference model for data transformation.
- use prediction of MultinomialNB to be entered and processed for EEMD.

Testing, Plotting and T-Test (MultinomialNB and EEMD)
    Testing
        - Input test data acquired from Data Gathering ()
    
    Plotting
        - Define plot model instance 
        - Define result values for MultinomialNB, EEMD and Average.
        - For each of the defined values, define model instances for plotting.
        - Include defined values in defined plot model instance

    T-Test
        - Use Plotted Values result to define T-Test
        - Define T-test in plot model instance
    
    -Plot and Print Plot Model Instance

|
v

- Output

END