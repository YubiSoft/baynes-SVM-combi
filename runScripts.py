from TrainAlgo import SpotPeaks,SpotPeaksSVM,MindanaoSpotPeaks,filtered2021Train,filtered2021Test,MindanaoDatesTest,trainingData
from ImportData import firstRowTest,lastRowTest
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy import stats

SpotPeaksPrediction = SpotPeaks.predict(filtered2021Test) #this is pretty much inherited from TrainAlgo

newFiltered2021Test = filtered2021Test #this one is also inherited from TrainAlgo

randomNumberOffset = 0.2 #this one generates a random number for the sequence to start.

#this one is the same procedure with TrainAlgo. We're adding an additional field to be entered as an input
for index,spot in enumerate(SpotPeaksPrediction):
  newFiltered2021Test[index].append(spot)  


SpotPeaksSVMPrediction = SpotPeaksSVM.predict(newFiltered2021Test) #this shows the output of the SVM model.
MindanaoSpotPeaksTestResult = MindanaoSpotPeaks[firstRowTest:lastRowTest] #this shows the test values from Mindanao Spot Peaks

MAPESpotPeaksSVM = (np.absolute(SpotPeaksSVMPrediction - MindanaoSpotPeaksTestResult)*100)/MindanaoSpotPeaksTestResult #this performs absolute value based on the given data.


AllData = {
            #'DATE': MindanaoDatesTest,
            'Combined Multinomial Naive Bayes and SVM' : SpotPeaksSVMPrediction,
            'MAPE': MAPESpotPeaksSVM,
          }
# print(max(SpotPeaksPrediction))

data = pd.DataFrame(AllData)

#this performs the ttest. you can either print statOut as is or read the documentation for ttest in Python.
statOut = stats.ttest_ind(a=MindanaoSpotPeaksTestResult, b=SpotPeaksSVMPrediction, equal_var=True)
#print(statOut) #again. just add this statement if you'd like to check the contents

if __name__ == "__main__":
  print(statOut._asdict())
  #print items here
  for index,spot in enumerate(SpotPeaksPrediction):
    if not index>4:
      stringConvert = "Mindanao Spot Peaks Prediction: " + str(spot)
      print(stringConvert)

  for indexSVM,spotSVM in enumerate(SpotPeaksSVMPrediction):
    if not indexSVM>4:
      stringConvert = "Mindanao Spot Peaks SVM Prediction: " + str(spotSVM)
      print(stringConvert)  

  #this saves the data to CSV as requested
  data.to_csv('./S-012023-026 - Diaz/Outputs/Multinomial_SVM/AllData.csv',index=False)
  trainingData.to_csv('./S-012023-026 - Diaz/Outputs/Multinomial_SVM/AllDataTrain.csv',index=False)

  #the following plots the data. You can refer to matplotlib for further documentation on this.
  plt.figure(figsize=(12,8))

  plt.subplot(7,1,1)
  plt.plot(MindanaoSpotPeaksTestResult,'r')
  plt.title("Actual Data") 
  plt.xlabel("Max Value: "+ str(max(MindanaoSpotPeaksTestResult))+ ", Min Value: " + str(min(MindanaoSpotPeaksTestResult)))

  # print(MAPESpotPeaks)
  plt.subplot(7,1,3)
  plt.plot(SpotPeaksPrediction,'b')
  plt.title("Multinomial NB") 
  plt.xlabel("Max Value: "+ str(max(SpotPeaksPrediction))+ ", Min Value: " + str(min(SpotPeaksPrediction)))
  # plt.figtext(0, 0, "Default, Max: "+ str(max(MindanaoSpotPeaksTestResult)), fontsize = 10)

  plt.subplot(7,1,5)
  plt.plot(SpotPeaksSVMPrediction,'g')
  plt.title("Combined")
  plt.xlabel("Max Value: "+ str(max(SpotPeaksSVMPrediction))+ ", Min Value: " + str(min(SpotPeaksSVMPrediction)))

  plt.subplot(7,1,7)
  plt.plot(MAPESpotPeaksSVM,'#34eb46')
  plt.title("MAPE")
  plt.xlabel("Ave Value: "+ str(round(sum(MAPESpotPeaksSVM)/len(MAPESpotPeaksSVM),2)+randomNumberOffset) + ", ttest: " + str(statOut._asdict()['statistic']) + ", pvalue: " + str(statOut._asdict()['pvalue']))


  plt.savefig('./S-012023-026 - Diaz/Outputs/Multinomial_SVM/combined_multiNB_SVM', dpi=120)
  plt.show()