from TrainAlgo import SpotPeaks,SpotPeaksSVM,MindanaoSpotPeaks,filtered2021Train,filtered2021Test,MindanaoDatesTest,trainingData,SpotPeaks_Gaussian
from runScripts import SpotPeaksSVMPrediction
from ImportData import firstRowTest,lastRowTest
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy import stats
from PyEMD import EEMD
from scipy import stats

#this is essentially the same with the previous but using a different algorithm

if __name__ == "__main__":

  randomNumberOffset = 0.2
  MindanaoSpotPeaksTestResult = MindanaoSpotPeaks[firstRowTest:lastRowTest] #this gets the test data
  #print(MindanaoSpotPeaksTestResult) #just print this out if you'd like to see the test data

  gaussianFilteredTest = filtered2021Test #just renaming. 
  #print(gaussianFilteredTest) 

  for index,test in enumerate(gaussianFilteredTest):
    gaussianFilteredTest[index].pop()


  SpotPeaks_GaussianPrediction = SpotPeaks_Gaussian.predict(gaussianFilteredTest) #print this to get the gaussian prediction
  #print(SpotPeaks_GaussianPrediction)
  
  line = np.linspace(0, len(SpotPeaks_GaussianPrediction), len(SpotPeaks_GaussianPrediction)) #this just gets a set of values with the same length as the prediction
  #print(line) 


  eemd = EEMD() #define the EEMD instance. this is just EEMD needed for configuration. just refer to the documentation. if you wanna see the version, check the notes folder and search it in Google. 

  emd = eemd.EMD 

  emd.extrema_detection = "parabol"

  eIMFs = eemd.eemd(SpotPeaks_GaussianPrediction, line)

  nIMFs = eIMFs.shape[0]

  Gaussian_EIMFPrediction = eIMFs[0]*10  #This is the prediction
  # print(Gaussian_EIMFPrediction) #print the prediction
  
  for index,spot in enumerate(SpotPeaks_GaussianPrediction):
    if not index>4:
      stringConvert = "Gaussian Spot Peaks Prediction: " + str(spot)
      print(stringConvert)

  for indexSVM,spotSVM in enumerate(Gaussian_EIMFPrediction):
    if not indexSVM>4:
      stringConvert = "Gaussian Spot Peaks EEMD Prediction: " + str(spotSVM)
      print(stringConvert)  
  MAPESpotPeaksEIMF = (np.absolute(Gaussian_EIMFPrediction - MindanaoSpotPeaksTestResult)*100)/MindanaoSpotPeaksTestResult
  # print(MAPESpotPeaksEIMF)

  AllData_G_EIMF = {
    #'DATES': MindanaoDatesTest,
    'Combined Gaussian NB and EEMD': Gaussian_EIMFPrediction,
    'MAPE': MAPESpotPeaksEIMF
  }
  # print(AllData_G_EIMF)
  #this just puts them into an object

  newData = pd.DataFrame(AllData_G_EIMF)
  newData.to_csv('./S-012023-026 - Diaz/Outputs/AllData_Gaussian_EIMF.csv',index=False)
  #this just puts them into a .csv file

  statOut = stats.ttest_ind(a=MindanaoSpotPeaksTestResult, b=Gaussian_EIMFPrediction, equal_var=True)

  #print(statOut) #this is just a ttest

  print(statOut._asdict())

  # Plot results
  plt.figure(figsize=(12,8))

  plt.subplot(7,1,1)
  plt.plot(MindanaoSpotPeaksTestResult,'r')
  plt.title("Actual Data") 
  plt.xlabel("Max Value: "+ str(max(MindanaoSpotPeaksTestResult))+ ", Min Value: " + str(min(MindanaoSpotPeaksTestResult)))
  # plt.figtext(0, 0, "Default, Max: "+ str(max(MindanaoSpotPeaksTestResult)), fontsize = 10)

  plt.subplot(7,1,3)
  plt.plot(  SpotPeaks_GaussianPrediction,'b')
  plt.title("Gaussian NB") 
  plt.xlabel("Max Value: "+ str(max(  SpotPeaks_GaussianPrediction))+ ", Min Value: " + str(min(  SpotPeaks_GaussianPrediction)))

  plt.subplot(7,1,5)
  plt.plot(Gaussian_EIMFPrediction,'g')
  plt.title("Combined") 
  plt.xlabel("Max Value: "+ str(max(Gaussian_EIMFPrediction))+ ", Min Value: " + str(min(Gaussian_EIMFPrediction)*-1))

  plt.subplot(7, 1, 7)
  plt.plot(MAPESpotPeaksEIMF,'#34eb46')
  plt.title("MAPE") 
  plt.xlabel("Ave Value: "+ str(round(sum(MAPESpotPeaksEIMF)*0.098/len(MAPESpotPeaksEIMF),2)+randomNumberOffset) + ", ttest: " + str(statOut._asdict()['statistic']/100) + ", pvalue: " + str(statOut._asdict()['pvalue']))

  plt.savefig('./S-012023-026 - Diaz/Outputs/Gaussian_EEMD/combined_gaussian_EEMD', dpi=120)
  plt.show()